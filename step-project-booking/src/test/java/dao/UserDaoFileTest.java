package dao;

import entity.User;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoFileTest {

    private File existingFile = new File("users.dat");
    private File notExistingFile = new File("users_not_existing.dat");
    @Test
    void test_getAll_when_file_not_found() {
        if (notExistingFile.exists()) {
            notExistingFile.delete();
        }
        List<User> allUsers = new UserDaoFile(notExistingFile).getAll();
        assertEquals(new ArrayList<User>(), allUsers);
        assertEquals(0,allUsers.size());
    }
    @Test
    void test_getAll_when_file_exist() {
        if (notExistingFile.exists()) {
            notExistingFile.delete();
        }
        UserDaoFile userDaoFile = new UserDaoFile(notExistingFile);

        User user1 = new User("user1", "user1");
        userDaoFile.save(user1);
        User user2 = new User("user2", "user2");
        userDaoFile.save(user2);

        List<User> allUsersExpect = List.of(user1, user2);
        List<User> allUsers = new UserDaoFile(notExistingFile).getAll();
        assertEquals(allUsersExpect, allUsers);
        assertEquals(2,allUsers.size());
    }

    @Test
    void test_getByIndex_when_file_not_found() {
        UserDaoFile userDaoFile = new UserDaoFile(notExistingFile);
        assertNull(userDaoFile.getByIndex(0));

    }
    @Test
    void test_getByIndex_when_file_exist() {
        UserDaoFile userDaoFile = new UserDaoFile(existingFile);
        User user7 = new User("user7", "pass7");
        userDaoFile.save(user7);
        System.out.println(user7.id());
        int id = user7.id();
        assertEquals(user7, userDaoFile.getByIndex(id));

    }

    @Test
    void test_deleteByID_when_file_not_found() {
        if (notExistingFile.exists()) {
            notExistingFile.delete();
        }
        UserDaoFile userDaoFile = new UserDaoFile(notExistingFile);
        assertFalse(userDaoFile.delete(0));

    }
    @Test
    void test_deleteByObject_when_file_not_found() {
        if (notExistingFile.exists()) {
            notExistingFile.delete();
        }
        UserDaoFile userDaoFile = new UserDaoFile(notExistingFile);
        User user7 = new User("user7", "pass7");
        assertFalse(userDaoFile.delete(user7));

    }
    @Test
    void test_deleteById_when_file_exist() {
        UserDaoFile userDaoFile = new UserDaoFile(existingFile);
        User user7 = new User("user7", "pass7");
        userDaoFile.save(user7);
        System.out.println(user7.id());

        assertTrue(userDaoFile.delete(user7));
        assertNull(userDaoFile.getByIndex(user7.id()));

    }

}