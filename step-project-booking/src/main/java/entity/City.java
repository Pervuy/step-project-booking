package entity;

public enum City {
    KYIV("Kyiv"),
    TOKYO("Tokyo"),
    LONDON("London"),
    NEW_YORK_CITY("New York City"),
    BEIJING("Beijing"),
    PARIS("Paris"),
    SEOUL("Seoul"),
    MOSCOW("Moscow"),
    ISTANBUL("Istanbul"),
    BERLIN("Berlin"),
    MADRID("Madrid"),
    ROME("Rome"),
    CAIRO("Cairo"),
    SYDNEY("Sydney"),
    TEHRAN("Tehran"),
    RIYADH("Riyadh"),
    LIMA("Lima"),
    BRASILIA("Brasilia"),
    OTTAWA("Ottawa"),
    ATHENS("Athens"),
    BANGKOK("Bangkok"),
    VIENNA("Vienna"),
    NAIROBI("Nairobi"),
    WARSAW("Warsaw"),
    DUBLIN("Dublin"),
    PRAGUE("Prague"),
    BUDAPEST("Budapest"),
    COPENHAGEN("Copenhagen"),
    HELSINKI("Helsinki"),
    LISBON("Lisbon"),
    BUCHAREST("Bucharest"),
    OSLO("Oslo"),
    BELGRADE("Belgrade"),
    SOFIA("Sofia"),
    BRUSSELS("Brussels"),
    AMSTERDAM("Amsterdam"),
    STOCKHOLM("Stockholm"),
    JAKARTA("Jakarta"),
    HANOI("Hanoi"),
    MANILA("Manila"),
    DHAKA("Dhaka"),
    KARACHI("Karachi"),
    KOLKATA("Kolkata"),
    MUMBAI("Mumbai"),
    LAGOS("Lagos"),
    JOHANNESBURG("Johannesburg"),
    ADDIS_ABABA("Addis Ababa");

    private final String name;

    City(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

}
