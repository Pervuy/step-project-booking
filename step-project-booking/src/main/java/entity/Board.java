package entity;

import java.io.Serializable;
import java.util.*;
import java.util.stream.*;

public class Board implements Serializable {
    Map<Integer, List<Place>> places;

    private static final int MAX_CAPACITY = Seat.values().length * 6;
    private static final int MIN_CAPACITY = Seat.values().length * 3;
    private final int capacity;
    public Board(int capacity) {
        int placesNum = capacity > MAX_CAPACITY
                ? MAX_CAPACITY
                : capacity < MIN_CAPACITY
                ? MIN_CAPACITY
                : (capacity / Seat.values().length) * Seat.values().length;
        this.places = IntStream.rangeClosed(1, placesNum / Seat.values().length)
                .boxed()
                .collect(Collectors.toMap(
                        i -> i,
                        i -> Arrays.stream(Seat.values())
                                .map(seat -> new Place(i, seat))
                                .collect(Collectors.toList())
                ));
        this.capacity = (int) places.values().stream()
                .mapToLong(List::size)
                .sum();
    }

    public int countVacantSeats() {
        return (int) places.values().stream()
                .flatMap(List::stream)
                .filter(Place::isVacant)
                .count();
    }

    public int countBookedSeats() {
        return (int) places.values().stream()
                .flatMap(List::stream)
                .filter(place -> !place.isVacant())
                .count();
    }

    public String showBoardPlacesCount() {
        return String.format("Vacant : %d\nBooked : %d\nTotal : %d", countVacantSeats(), countBookedSeats(), capacity);
    }

    public String showVacantSeats() {
        return places.values().stream()
                .flatMap(Collection::stream)
                .filter(Place::isVacant)
                .collect(Collectors.toMap(Place::getRow,
                        Place::toString,
                        (s1, s2) -> s1 + " " + s2))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(entry -> String.format("%d: %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining("\n", "Vacant seats:\n", ""));
    }


    public Map<Integer, List<Place>> getPlaces() {
        return places;
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean bookPlace(int row, Seat seat) {
        return this.places.get(row)
                .stream()
                .filter(place -> place.getSeat() == seat)
                .findFirst().orElseThrow().bookPlace();
    }

    public boolean cancellation(int row, Seat seat) {
        return this.places.get(row)
                .stream()
                .filter(place -> place.getSeat() == seat)
                .findFirst().orElseThrow().setVacant();
    }

    public static int getMaxCapacity() { return MAX_CAPACITY; }

    public static int getMinCapacity() { return MIN_CAPACITY; }
}
