package entity;

import constants.Constants;
import dao.Identifiable;
import dao.UserDaoFile;


import java.io.File;
import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable, Identifiable{

    public static int maxId = new UserDaoFile(new File(Constants.USERS_FILE_DB)).getMaxIdUser();
    private final int id;
    private final String name;
    private String password;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
        User.maxId++;
        id = User.maxId;
    }
    public User(String name){
        this(name, "");
    }

    @Override
    public int id() {
        return id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public String getName() {
        return name;
    }
}
