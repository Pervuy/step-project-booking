package entity;

public enum Airline {

    UKRAINE_INTERNATIONAL_AIRLINES("UI", "Ukraine International Airlines"),
    WIZZ_AIR("W6", "Wizz Air"),
    RYANAIR("FR", "Ryanair"),
    TURKISH_AIRLINES("TK", "Turkish Airlines"),
    LOT_POLISH_AIRLINES("LO", "LOT Polish Airlines"),
    LUFTHANSA("LH", "Lufthansa"),
    BELAVIA("B2", "Belavia"),
    AUSTRIAN_AIRLINES("OS", "Austrian Airlines"),
    AIR_FRANCE("AF","Air France"),
    KLM_ROYAL_DUTCH_AIRLINES("KL","KLM Royal Dutch Airlines"),
    QATAR_AIRWAYS("QR","Qatar Airways"),
    EMIRATES("EK","Emirates"),
    UIA_MERIDIAN("UF", "UIA-Meridian"),
    PEGASUS_AIRLINES("PC","Pegasus Airlines"),
    ERNEST_AIRLINES("EG","Ernest Airlines"),
    AIRBALTIC("BT","AirBaltic"),
    SKYUP_AIRLINES("PQ","SkyUp Airlines");

    private final String code;
    private final String name;
    Airline(String code, String name) {
        this.code = code;
        this.name = name;
    }
    public String getCode(){
        return this.code;
    }
    public String getName(){
        return this.name;
    }
}
