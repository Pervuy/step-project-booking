package entity;

public enum Seat {
    A("Window Seat"),
    B("Middle Seat"),
    C("Aisle Seat"),
    D("Aisle Seat"),
    E("Middle Seat"),
    F("Window Seat");

    private final String seatName;

    Seat(String seatName) {
        this.seatName = seatName;
    }

    public String getSeatName(){
        return this.seatName;
    }
}
