package entity;

import dao.Identifiable;
import libs.EX;

import java.io.Serializable;
import java.time.*;
import java.time.format.*;

public class Flight implements Serializable, Identifiable {
    private final int id;
    private final Airline airline;
    private final City departure;
    private final City destination;
    private final LocalDate flightDate;
    private final LocalTime flightTime;
    private final int capacity;
    private Board board;

    public Flight(Airline airline, City departure, City destination, LocalDate flightDate, LocalTime flightTime, int capacity, int id) {
        this.airline = airline;
        this.departure = departure;
        this.destination = destination;
        this.flightDate = flightDate;
        this.flightTime = flightTime;
        this.board = new Board(capacity);
        this.capacity = board.getCapacity();
        this.id = id;
    }

    private String flightKeyGenerator(Flight flight) {
        StringBuilder sb = new StringBuilder();
        return sb.append(flight.getAirline().getCode())
                .append(flight.getDeparture().getName(), 0, 2)
                .append(flight.getDestination().getName(), 0, 2)
                .append(flight.getFlightDate().format(DateTimeFormatter.ofPattern("ddMMyy")))
                .append(flight.getFlightTime().format(DateTimeFormatter.ofPattern("HHmm")))
                .toString()
                .toUpperCase();
    }

    public String prettyFormat() {
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("HH:mm");
        return String.format("%4d %4s %13s - %-13s %12s %6s",
                id(), getAirline().getCode(), getDeparture().getName(), getDestination().getName(),
                getFlightDate().format(formatter1), getFlightTime().format(formatter2));
    }

    public Airline getAirline() {
        return airline;
    }

    public City getDeparture() {
        return departure;
    }

    public City getDestination() {
        return destination;
    }

    public LocalDate getFlightDate() {
        return flightDate;
    }

    public LocalTime getFlightTime() {
        return flightTime;
    }

    public int getCapacity() { return capacity; }

    public Board getBoard() {
        return board;
    }

    @Override
    public int id() {
        return id;
    }
}
