package service;

import constants.Constants;
import dao.UserDaoFile;
import entity.User;

import java.io.File;

public class UserService {
    private final UserDaoFile userDaoFile = new UserDaoFile(new File(Constants.USERS_FILE_DB));

    public void save(User user){
        userDaoFile.save(user);
    }
    public User getUserByName(String name){
        return userDaoFile
                .getAll()
                .stream()
                .filter(user -> user.getName().equals(name))
                .findAny()
                .orElse(new User(name));
    }

}
