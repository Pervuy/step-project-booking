package dao;

import java.util.List;

public interface DAO<T extends Identifiable> {

    List<T> getAll();
    T getByIndex(int id);
    boolean delete(int id);
    default boolean delete(T t){
        return delete(t.id());
    }
    void save(T t);

}
