package dao;

import entity.Flight;
import libs.EX;

import java.io.*;
import java.util.*;

public class FlightDao implements DAO<Flight>, Serializable {
    String filePath = System.getProperty("user.dir") + File.separator + "flights.dat";
    private File file = new File(filePath);
    private List<Flight> flights;

    public FlightDao() {
        flights = new ArrayList<>();
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch(IOException e) {
            throw EX.ECREATF;
        }
    }

    @Override
    public List<Flight> getAll() {
        List<Flight> flights = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            flights = (List<Flight>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {}
        return flights;
    }

    @Override
    public Flight getByIndex(int id) {
        try {
            return flights.get(id);
        } catch (IndexOutOfBoundsException e) {
            throw EX.EGETIND;
        }
    }

    @Override
    public boolean delete(int id) {
        boolean isDeleted = flights.remove(flights.get(id));
        loadData(flights);
        return isDeleted;
    }

    @Override
    public void save(Flight flight) {
        if (!flights.isEmpty() && flights.contains(flight)) {
            int index = flights.indexOf(flight);
            flights.set(index, flight);
        } else
            flights.add(flight);
        loadData(flights);
    }

    public void loadData(List<Flight> flightsToLoad) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(flightsToLoad);
        } catch (IOException e) {
            throw EX.EWRITEF;
        }
    }
}
