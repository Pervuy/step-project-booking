package FlightGenerator;

import dao.FlightDao;
import entity.Airline;
import entity.Board;
import entity.City;
import entity.Flight;
import service.FlightService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

public class Generator {
    Random random = new Random();
    static final int MAX_FLIGHTS_PER_DAY = 4;
    LocalDate startDate = LocalDate.now();
    LocalDate endDate = startDate.plusWeeks(2);
    FlightService flightService;

    public Generator() {
        flightService = new FlightService();
        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            for ( int i = 0; i < MAX_FLIGHTS_PER_DAY; i++) {
                Airline airline = Airline.values()[random.nextInt(Airline.values().length)];
                City departure = City.values()[random.nextInt(City.values().length)];
                City destination = Arrays.stream(City.values())
                        .filter(x -> !x.equals(departure))
                        .toList()
                        .get(random.nextInt(City.values().length - 1));
                LocalTime time = LocalTime.of(random.nextInt(24), random.nextInt(12) * 5);
                int capacity = random.nextInt(Board.getMaxCapacity() - Board.getMinCapacity() + 1) + Board.getMinCapacity();
                flightService.createNewFlight(airline, departure, destination, date, time, capacity);
            }
        }
    }

    public List<Flight> getFlightData() {
        return flightService.getAllFlights();
    }
}
